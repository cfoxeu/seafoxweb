var express = require('express');
var router = express.Router();

// GET homepage
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Seafox Web - Home' });
});

// Get other routes
router.get('/about', function(req, res, next) {
    res.render('pages/about', { title: 'About - Seafox Web' });
});
router.get('/portfolio', function(req, res, next) {
  res.render('pages/portfolio', { title: 'Portfolio - Seafox Web' });
});
router.get('/pricing', function(req, res, next) {
  res.render('pages/pricing', { title: 'Pricing - Seafox Web' });
});
router.get('/contact', function(req, res, next) {
  res.render('pages/contact', { title: 'Contact - Seafox Web' });
});

module.exports = router;
